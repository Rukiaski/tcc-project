# MODIFICAÇÕES


## 5 de outubro:
	- Modificado o arquivo items.py:
		- Apagada a configuração padrão;
		- Configurado os itens que seriam utilizados na spider.
	- Modificado o arquivo pipeline.py:
		- Apagada a configuração padrão;
		- Importado o pacote json e configurado para salvar os itens extraídos nesse formato.
	- Modificado o tipo do seletor css para o xpath das chaves date e valor.
	- Corrigido ruído de quantidade de datas maior que a quantidade de comentários.
	- Verificado que a condicional feita no parse_forum não funcionava para passar para as pŕoximas páginas. É necessário ver uma forma de fazer a passagem das páginas sem bugar ou perder o conteúdo.

## 8 de outubro:
	- Trocado todos os seletores para utilizar o XPATH;
	- Deixado os seletores css em Rules pois o xpath não estava funcionando;
	- Retirado o download_delay;
	- Modificado retirada a regra referente a próxima página:
		- Feito parse_forum para fazer a função da regra excluída
		- Feito o parse_item para salvar os itens
		(Ainda sem funcionar como deveria)

## 9 de outubro:
	- Recolocado o download_delay;
	- Colocada regra para fazer o seguimento das páginas dos post e não das postagens;
	(Aparentemente funcionou, pois estava passando e pegando post cada vez mais antigos)
	- Excluído o parse_forum;
	- Implementada a parte de passar as postagens do post;
	(Aparentemente funcionando)

## 17 de outubro:
	- Adicionado em items:
		- category
		- subcategory
		- plataform
		- nickaname

## 30 de outubro:
	- Modificada a forma de coleta dos dados para coletá-los formatados. No entanto, quando utilizado extract_first, retorna os dados no formato correto, mas só coleta o primeiro dado de cada página. E quando usado somente extract, coleta todos os dados mas em formato de array;
	- Deletado warframeforum;
	- Será feita uma spider para cada subategoria de feedback;
	- Spider feedback_sound:
		- Está pegando os dados do primeiro comentário de cada página;
		- Problemas para pegar url_comentário (verificar depois);
		- Executada e dados salvos em feedback_sound.csv.

## 31 de outubro:
	- Criada e executadas spiders: feedback_conclave, feedback_art, feedback_missions

## 5 de novembro:
	- Criada todas as spiders para cada subcategoria;
	- Coletando os dados já formatados e sem loop.
