# -*- coding: utf-8 -*-
from typing import List

import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Rule, CrawlSpider
from warframe.items import WarframeItem


class FeedbackWeaponsSpider(CrawlSpider):
    name = 'feedback_weapons'
    download_delay = 0.5
    allowed_domains = ['forums.warframe.com']

    start_urls = [
        'http://forums.warframe.com/forum/76-weapons/',
    ]

    rules = (
        Rule(
            LinkExtractor(restrict_css=['li.ipsPagination_next']),
            follow=True,
        ),
        Rule(
            LinkExtractor(restrict_css=['span.ipsType_break.ipsContained']),
            callback='parse_item',
        ),
    )

    def parse_item(self, response):
        for comment in response.css('article[id]'):
            yield {
                'category': 'feedback',
                'sub_category': 'weapons',
                'title': comment.xpath('//div/h1/span/span/text()').extract_first(),
                'date': comment.xpath('.//div[@class="ipsType_reset"]/a/time/@title').extract_first(),
                'platform': comment.xpath('.//li/span[contains(@style,"color")]/text()').extract_first(),
                'nickname': comment.xpath('.//a/span[contains(@style,"color")]/text()').extract_first(),
                'rank': comment.xpath('.//ul/li[@class="ipsType_break"]/text()').extract_first(),
            }

        next_page = response.css('li.ipsPagination_next a::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse_item)




